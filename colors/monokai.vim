" Vim colorscheme file
"
" Author: Joseph M LaFreniere <joseph@lafreniere.xyz>
" https://gitlab.com/lafrenierejm/monokai
"
" Credit: Based on the Monokai theme for TextMate by Wimer Hazenberg
" http://www.monokai.nl/blog/2006/07/15/textmate-color-theme/

highlight clear
if exists('syntax_on')
	syntax reset
endif
let g:colors_name='monokai'

" groups from syntax.txt
highlight ColorColumn  ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Comment      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#76715E guibg=bg      guisp=NONE gui=NONE
highlight Conceal      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Constant     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#CFD0C2 guibg=bg      guisp=NONE gui=NONE
highlight Cursor       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight CursorColumn ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight CursorIM     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight CursorLine   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=NONE    guibg=NONE    guisp=NONE gui=NONE
highlight CursorLineNr ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#E7DB75 guibg=bg      guisp=NONE gui=NONE
highlight DiffAdd      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=#8FC029 guisp=NONE gui=NONE
highlight DiffChange   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=#56B7A5 guisp=NONE gui=NONE
highlight DiffDelete   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=#9358FE guisp=NONE gui=NONE
highlight DiffText     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#F1EBEB guibg=bg      guisp=NONE gui=NONE
highlight Directory    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight EndOfBuffer  ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#48483E guibg=bg      guisp=NONE gui=NONE
highlight Error        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#FA2772 guibg=bg      guisp=NONE gui=bold
highlight ErrorMsg     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#FA2772 guibg=bg      guisp=NONE gui=NONE
highlight FoldColumn   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#CFD0C2 guibg=#48483E guisp=NONE gui=NONE
highlight Folded       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#CFD0C2 guibg=#48483E guisp=NONE gui=NONE
highlight Identifier   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#66D9EE guibg=bg      guisp=NONE gui=NONE
highlight Ignore       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight IncSearch    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#48483E guibg=#D4C96E guisp=NONE gui=NONE
highlight LineNr       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#48483E guibg=bg      guisp=NONE gui=NONE
highlight MatchParen   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#E7DB75 guibg=bg      guisp=NONE gui=NONE
highlight Menu         ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight ModeMsg      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight MoreMsg      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight NonText      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#48483E guibg=bg      guisp=NONE gui=NONE
highlight Normal       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#F1EBEB guibg=#272822
highlight Pmenu        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight PmenuSbar    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight PmenuSel     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight PmenuThumb   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight PreProc      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#AE82FF guibg=bg      guisp=NONE gui=NONE
highlight Question     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight QuickFixLine ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Scrollbar    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Search       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#E7DB75 guibg=NONE    guisp=NONE gui=NONE
highlight SignColumn   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight SpecialKey   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#48483E guibg=bg      guisp=NONE gui=NONE
highlight SpellBad     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#FA2772 guibg=bg      guisp=NONE gui=undercurl
highlight SpellCap     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#FA2772 guibg=bg      guisp=NONE gui=undercurl
highlight SpellLocal   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight SpellRare    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Statement    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#F1EBEB guibg=bg      guisp=NONE gui=bold
highlight StatusLine   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#ACADA1 guibg=#48483E guisp=NONE gui=NONE
highlight StatusLineNC ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#ACADA1 guibg=#272822 guisp=NONE gui=NONE
highlight Substitute   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#E7DB75 guibg=NONE    guisp=NONE gui=NONE
highlight TabLine      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight TabLineFill  ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight TabLineSel   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight TermCursor   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight TermCursorNC ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Title        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Todo         ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Tooltip      ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Type         ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#F1EBEB guibg=bg      guisp=NONE gui=italic
highlight Underlined   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User1        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User2        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User3        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User4        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User5        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User6        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User7        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User8        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight User9        ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight VertSplit    ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight Visual       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=NONE    guibg=#403D3D guisp=NONE gui=NONE
highlight WarningMsg   ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=fg      guibg=bg      guisp=NONE gui=NONE
highlight WildMenu     ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#E7DB75 guibg=#48483E guisp=NONE gui=NONE

" groups from sh.vim
highlight link shDerefWordError Normal

" groups from vim-gutgutter (https://github.com/airblade/vim-gitgutter)
highlight GitGutterAdd          ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#8FC029 guibg=NONE    guisp=NONE gui=NONE
highlight GitGutterChange       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#56B7A5 guibg=NONE    guisp=NONE gui=NONE
highlight GitGutterChangeDelete ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#9358FE guibg=NONE    guisp=NONE gui=NONE
highlight GitGutterDelete       ctermfg=NONE    ctermbg=NONE    cterm=NONE guifg=#9358FE guibg=NONE    guisp=NONE gui=NONE
